

#include <boost/bind.hpp>
#include <iomanip>
#include <iostream>
#include <algorithm>

#include "client.h"


using namespace Program;


typedef std::shared_ptr<io_service> service_ptr;
typedef std::shared_ptr<ip::tcp::socket> socket_ptr;


class Program::ClientImpl
{
public:
    ClientImpl();
    
    service_ptr& service();
    socket_ptr& socket();
    std::fstream& csv();
    std::fstream& report();
    std::string& data();
    records_vector& records();

private:
    service_ptr _service;
    socket_ptr _socket;
    std::fstream _csv, _report;
    std::string _data;
    records_vector _records;
    
};


ClientImpl::ClientImpl()
    :   _service( nullptr ),
        _socket( nullptr )
{
    _service = std::make_shared<io_service>();
    _socket = std::make_shared<ip::tcp::socket>( *_service );
    _data.resize(BUFF_SIZE);
}


service_ptr& ClientImpl::service()
{
    return _service;
}

socket_ptr& ClientImpl::socket()
{
    return _socket;
}

std::fstream& ClientImpl::csv()
{
    return _csv;
}

std::fstream& ClientImpl::report()
{
    return _report;
}

std::string& ClientImpl::data()
{
    return _data;
}

records_vector& ClientImpl::records()
{
    return _records;
}


Client::Client()
    : _impl( std::make_shared<ClientImpl>() )
{
}

Client::~Client()
{
    close();
}


void Client::start( const std::string& addr_str, const std::string& port_str, const std::string& file_name )
{
    _impl->csv().open( file_name, std::ios_base::in );
    if( ! _impl->csv().is_open() ) 
	{
        std::cerr << "error: csv file does not exist" << std::endl;
		return;
    }
    try {
        _impl->records() = read_records( _impl->csv() );
    }
    catch( std::logic_error& ex )
	{
		std::cerr << "error: file is invalid" << std::endl;
		return;
	}
    if( _impl->records().empty() )
    {
        std::cerr << "error: file is empty" << std::endl;
        return;
    }
    
    _impl->report().open( "report.txt", std::ios_base::out );
    if( ! _impl->report().is_open() ) 
	{
        std::cerr << "error: report file is not available" << std::endl;
		return;
    }    
	
    ip::address addr;
	if( addr_str != "localhost" )
    {
		error_code err;
		addr = ip::address::from_string( addr_str, err );
		if( err ) 
		{
			std::cerr << "error: address is invalid"  << std::endl;
			return;
		}
	}
	else
		addr = ip::address::from_string( "127.0.0.1" );
	
    std::stringstream ss( port_str );
    unsigned short port;
    ss >> port;
    if( ss.fail() )
	{
        std::cerr << "error: port is invalid" << std::endl;
		return;
    }
    
    connect( ip::tcp::endpoint( addr, port ) );
    _impl->service()->run();
}


void Client::receive_complete( const error_code& err, size_t total_len )
{
    if( err )
    {
        close();
        return;
    }
    
    if( total_len == BUFF_SIZE && _impl->socket()->available() > 0 )
    {
        std::string buff(BUFF_SIZE, 0);
        while( _impl->socket()->available() > 0 && _impl->data().size() < MAX_DATA_SIZE )
        {
            size_t len = _impl->socket()->receive( buffer(buff) );
            _impl->data().insert( _impl->data().end(), buff.begin(), buff.begin() + len );
        }
    }
    else
        _impl->data().resize(total_len);
    
    std::stringstream ss( _impl->data() );
    
    size_t line_num;
    ss >> line_num;
    if( ! ss.fail() )
    {
        std::stringstream msg;
        msg << "number of lines is " << line_num;
        
        _impl->report() << msg.str() << std::flush;
        std::cout << "done! " << msg.str() << std::endl;
    }
    else 
    {
        _impl->report() << "server -> " << _impl->data() << "\n";
        std::cerr << "server -> " << _impl->data() << std::endl;
    }
}


void Client::receive()
{
    _impl->socket()->async_receive( buffer(_impl->data()), boost::bind( &Client::receive_complete, this, _1, _2 ) ); 
}


void Client::send_complete( const error_code& err, size_t total_len )
{
    if( err )
    {
		std::cerr << "error: sending error" << std::endl;
        close();
        return;
    }
    receive();
}


void Client::send( const std::string& str )
{
    _impl->socket()->async_send( buffer(str), boost::bind( &Client::send_complete, this, _1, _2 ) );
}


void Client::connect_complete( const error_code& err )
{
    if( err ) 
    {
		std::cerr << "error: server unavailable" << std::endl;
        close();
        return;
    }
    
    std::stringstream ss;
    records_vector& records = _impl->records();
    for( auto iter = records.begin(); iter != records.end(); ++iter ) {
        ss << std::put_time( (*iter)->_time.get(), "%d.%m.%Y %H:%M:%S" ) << ";" << (*iter)->_first << ";" << (*iter)->_second;
        if( iter != records.end() - 1 ) {
            ss << "\n";
        }
    }
    send( ss.str() );
}


void Client::connect( const ip::tcp::endpoint& endpoint )
{
    _impl->socket()->async_connect( endpoint, boost::bind( &Client::connect_complete, this, _1 ) );
}


void Client::close()
{
    _impl->csv().close();
    _impl->report().close();
    
    _impl->service()->stop();
    _impl->socket()->close();
}

