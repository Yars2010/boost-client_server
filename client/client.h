

#ifndef CLIENT_H
#define CLIENT_H


#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <fstream>

#include "read_records.h"


#define MAX_DATA_SIZE 65535
#define BUFF_SIZE 1024


namespace Program
{
    
    using namespace boost::asio;
    using namespace boost::system;
    
    
    class ClientImpl;
    
    typedef std::shared_ptr<ClientImpl> ClientImplPtr;
    
    
    class Client
    {
    public:
        Client();
        ~Client();
        
        Client( const Client& client ) = delete;

        void start( const std::string& addr_str, const std::string& port_str, const std::string& file_name );
        
    private:
        void connect_complete( const error_code& err );
        void connect( const ip::tcp::endpoint& endpoint );
        void receive_complete( const error_code& err, size_t total_len );
        void receive();
        void send_complete( const error_code& err, size_t total_len );
        void send( const std::string& str );
        void close();
    
        ClientImplPtr _impl;

    };
    
    
} // namespace Program 


#endif // CLIENT_H
