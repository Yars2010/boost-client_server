

#include <iostream>
#include <string>

#include "client.h"


using namespace Program;


int main(int argc, char **argv)
{
	if( argc == 4 )
	{	
		std::string ip = argv[1], port = argv[2], file_name = argv[3];
		Client client;
		client.start( ip, port, file_name );
	}
	else
		std::cout << "example usage: client 127.0.0.1 7777 csv_file.txt" << std::endl;
	
	return 0;
}
