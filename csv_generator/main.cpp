

#include <iostream>
#include <fstream>
#include <chrono>
#include <sstream>
#include <random>
#include <iomanip>


using namespace std::chrono;


int main(int argc, char **argv)
{
	if( argc == 3 )
    {
        std::string file_name = argv[1], rec_num = argv[2];
        std::stringstream ss( rec_num );
        
        size_t num;
        ss >> num;
        if( ss.fail() || num > 65535 ) 
        {
            std::cerr << "REC_NUM is invalid" << std::endl;
            return 1;
        }
        
        std::fstream file( file_name, std::ios_base::out );
        
        std::default_random_engine engine( system_clock::to_time_t(system_clock::now() ) );
        std::uniform_int_distribution<int> time_distr( 0, 2147483647 );
        std::uniform_real_distribution<float> float_distr( -65535.0, 65535.0 );
        
        for ( size_t i = 0; i < num; ++i ) 
        {
            time_t time = time_distr( engine );
            std::tm* tm = gmtime( &time );
            file << std::put_time( tm, "%d.%m.%Y %H:%M:%S" ) << ";" << 
                std::fixed << std::setprecision(5) << float_distr(engine) << ";" << 
                std::fixed << std::setprecision(5) << float_distr(engine);
                
            if( i != num - 1 )
                file << "\n";
        }
        
    }
    else
        std::cout << "usage example: generator file_name.txt REC_NUM";
        
    return 0;
}
