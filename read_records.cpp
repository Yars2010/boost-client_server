

#include <istream>
#include <iomanip>

#include "read_records.h"


using namespace Program;


class sep
{
public:
    sep( char ch ): _ch( ch ) { }
    friend std::istream& operator >> ( std::istream& stream, const sep& s )
    {
        char ch;
        stream >> ch;
        if( ch != s._ch ) {
            stream.setstate( std::ios_base::failbit );
        }
        return stream;
    }
    
private:
    char _ch;
};


records_vector Program::read_records( std::istream& stream )
{
    records_vector records;
    
    char ch;
    while( stream >> ch )
    {
        stream.putback(ch);
        
        auto time = std::make_shared<std::tm>();
        double first, second;
        
        stream >> std::get_time( time.get(), "%d.%m.%Y %H:%M:%S" ) >> sep(';') >> first >> sep(';') >> second;
        if( ! stream.fail() )
            records.push_back( std::make_shared<Record>( time, first, second ) );
        else
            throw std::logic_error( "error: invalid records" );
        
    }
    
    return records;
}