

#ifndef READ_RECORDS_H
#define READ_RECORDS_H


#include <vector>
#include <string>
#include <memory>
#include <ctime>


namespace Program
{
    
    struct Record
    {
        typedef std::shared_ptr<Record> ptr;
        
        Record( std::shared_ptr<std::tm> time, double first, double second )
            : _time( time ), _first( first ), _second( second ) { }
            
        std::shared_ptr<std::tm> _time;
        double _first;
        double _second;
    };
    
    
    typedef std::vector<Record::ptr> records_vector;
    
    
    records_vector read_records( std::istream& stream );
}

#endif // READ_RECORDS_H   