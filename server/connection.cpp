

#include <algorithm>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <ctime>

#include "connection.h"


using namespace Program;


Connection::Connection( Handler::ptr handler )
    :   _handler( handler ),
        _socket( nullptr )
{
    _socket = std::make_shared<ip::tcp::socket>( *handler->service() );
    _data.resize( BUFF_SIZE );
}


Connection::ptr Connection::new_instance( Handler::ptr handler )
{
    return std::make_shared<Connection>( handler );
}


void Connection::send_complete( const error_code& err, size_t total_len )
{
    if( !err )
        std::cout << "ok! client " << _socket->remote_endpoint() << " have been processed" << std::endl;
    else
        std::cerr << "error: sending error" << std::endl;

    work_complete( shared_from_this(), error_code() );
}


void Connection::send( const std::string& str )
{
    _socket->async_send( buffer(str), boost::bind(&Connection::send_complete, this, _1, _2 ) );
}


void Connection::receive_complete( const error_code& err, size_t total_len )
{
    if( err ) 
    {
        std::cerr << "error: receiving error" << std::endl;
        work_complete( shared_from_this(), error_code() );
        return;
    }
    
    if( total_len == BUFF_SIZE && _socket->available() > 0 )
    {
        std::string buff(BUFF_SIZE, 0);
        while( _socket->available() > 0 && _data.size() < MAX_DATA_SIZE )
        {
            size_t len = _socket->receive( buffer(buff) );
            _data.insert( _data.end(), buff.begin(), buff.begin() + len );
        }
        if( _data.size() >= MAX_DATA_SIZE )
        {
            send( "error: file is very large" );
            work_complete( shared_from_this(), error_code() );
            return;
        }
    }
    else
        _data.resize(total_len);
    
    records_vector records;
    try{
        std::stringstream ss( _data );
        records = read_records( ss );
    }
    catch( std::exception& ex )
    {
        send( std::string( ex.what() ) );
        work_complete( shared_from_this(), error_code() );
        return;
    }
    if( records.empty() )
    {
        send( "error: file is empty" );
        work_complete( shared_from_this(), error_code() );
        return;
    }
    
    write_protocol( records );
    
    std::stringstream ss;
    ss << records.size();

    send( ss.str() );
}


void Connection::receive()
{
    _socket->async_receive( buffer(_data), boost::bind(&Connection::receive_complete, this, _1, _2) );
}


void Connection::handle()
{
    receive();
}


void Connection::write_protocol( const std::vector<Record::ptr>& data )
{
    auto max_item = std::max_element( data.begin(), data.end(), [] ( Record::ptr r1, Record::ptr r2 ) 
    {
        time_t t1 = mktime(r1->_time.get()), t2 = mktime(r2->_time.get());
        return difftime( t1, t2 ) < 0;
    });
    std::stringstream ss;
    ss  << "ip: " << _socket->remote_endpoint() << ", " 
        << "line's number: " << std::distance( data.begin(), max_item ) << ", "
        << "max time: " << std::put_time( (*max_item)->_time.get(), "%d.%m.%Y %H:%M:%S" ) << ", " 
        << "difference: " << std::fixed << std::setprecision(5) << (*max_item)->_first - (*max_item)->_second << "\n"; 
        
    _handler->write_report( ss.str() );
}


void Connection::close()
{
    _socket->close();
}
