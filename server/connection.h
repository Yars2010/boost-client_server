

#ifndef CONNECTION_H
#define CONNECTION_H


#include <boost/asio/ip/tcp.hpp>
#include <boost/signals2/signal.hpp>
#include <memory>
#include <vector>

#include "handler.h"
#include "read_records.h"


#define MAX_DATA_SIZE 65535
#define BUFF_SIZE 1024


namespace Program
{
    
    using namespace boost::asio;
    using namespace boost::system;
    
    
    typedef std::shared_ptr<ip::tcp::socket> socket_ptr;
    
    
    class Connection: public std::enable_shared_from_this<Connection>
    {
    public:
        
        typedef std::shared_ptr<Connection> ptr;
        typedef boost::signals2::signal<void( Connection::ptr, const error_code& )> signal;
        
        Connection( Handler::ptr handler );
        
        static ptr new_instance( Handler::ptr handler );
        void handle();
        socket_ptr socket() { return _socket; };
        
        signal work_complete;
        
    private:
        void write_protocol( const std::vector<Record::ptr>& data );
        void receive_complete( const error_code& err, size_t total_len );
        void receive();
        void send_complete( const error_code& err, size_t total_len );
        void send( const std::string& str );
        void close();
        
        Handler::ptr _handler;
        socket_ptr _socket;
        std::string _data;

    };
    

} // namespace Program


#endif // CONNECTION_H
