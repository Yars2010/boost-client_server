

#include "handler.h"


using namespace Program;


Handler::ptr Handler::new_instance()
{
    return std::make_shared<Handler>();
}


Handler::Handler()
    :   _service( nullptr ), 
        _work( nullptr )
{
    _service = std::make_shared<io_service>();
    _work = std::make_shared<io_service::work>( *_service );
    
    size_t threads_num = std::thread::hardware_concurrency() > 1 ? std::thread::hardware_concurrency() - 1 : 1;
    _threads.reserve( threads_num );
    
    try {
        for( size_t i = 0; i < threads_num; ++i ) {
            _threads.push_back( std::thread( &Handler::threads_loop, this ) );
        }
    }
    catch( std::exception& )
    {
        close_all();
        throw;
    }
    _report.open( "report.txt", std::ios_base::out );
}

void Handler::write_report( const std::string& str )
{
    std::lock_guard<std::mutex> lock( _mutex );
    _report << str << std::flush;
}


Handler::~Handler()
{
    close_all();
    
    _report.close();
}


void Handler::close_all()
{
    _service->stop();
    
    for( size_t i = 0; i < _threads.size(); ++i ) 
    {
        if( _threads[i].joinable() )
            _threads[i].join();
    }
}


void Handler::threads_loop()
{
    _service->run();
}
