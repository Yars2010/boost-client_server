

#ifndef HANDLER_H
#define HANDLER_H


#include <boost/asio/io_service.hpp>
#include <fstream>
#include <thread>
#include <vector>


namespace Program
{
    
    using namespace boost::asio;
    
    
    typedef std::shared_ptr<io_service> io_service_ptr;
    typedef std::shared_ptr<io_service::work> work_ptr;
    typedef std::vector<std::thread> threads_vector;    


    class Handler
    {
    public:
        typedef std::shared_ptr<Handler> ptr;
        
        Handler();
        ~Handler();
        
        static ptr new_instance();
        void write_report( const std::string& str );
        io_service_ptr service() { return _service; }
        
    private:
        void threads_loop();
        void close_all();
    
        io_service_ptr _service;
        work_ptr _work;
        threads_vector _threads;
        std::fstream _report;
        std::mutex _mutex;
        
    };


} // namespace Program


#endif // HANDLER_H
