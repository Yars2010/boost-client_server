

#include <iostream>

#include "server.h"


using namespace Program;


int main(int argc, char **argv)
{
	if( argc == 3 )
	{
		std::string ip = argv[1], port = argv[2];
		Server server;
		server.start( ip, port );
	}
	else
		std::cout << "example usage: server 127.0.0.1 7777" << std::endl;
		
	return 0;
}
