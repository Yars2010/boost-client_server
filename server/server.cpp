

#include <boost/bind.hpp>
#include <mutex>
#include <sstream>
#include <iostream>
#include <memory>

#include "server.h"


using namespace Program;


typedef std::shared_ptr<ip::tcp::acceptor> acceptor_ptr;
typedef std::vector<Connection::ptr> connections_vector;


class Program::ServerImpl
{
public:
    ServerImpl();
    
    io_service_ptr& service();
    acceptor_ptr& acceptor();
    Handler::ptr& handler();
    connections_vector& connections();
    std::mutex& mutex();
    
private:
    io_service_ptr _service;
    acceptor_ptr _acceptor;
    Handler::ptr _handler;
    connections_vector _connections;
    std::mutex _mutex;

};


ServerImpl::ServerImpl()
    :   _service( nullptr ), 
        _acceptor( nullptr ), 
        _handler( nullptr )
{
    _service = std::make_shared<io_service>();
    _acceptor = std::make_shared<ip::tcp::acceptor>( *_service );
    _handler = Handler::new_instance();
}

io_service_ptr& ServerImpl::service()
{
    return _service;
}

acceptor_ptr& ServerImpl::acceptor()
{
    return _acceptor;
}

Handler::ptr& ServerImpl::handler()
{
    return _handler;
}

connections_vector& ServerImpl::connections()
{
    return _connections;
}

std::mutex& ServerImpl::mutex()
{
    return _mutex;
}


Server::Server()
    : _impl( std::make_shared<ServerImpl>() )
{
}


void Server::handle_complete( Connection::ptr connection, const system::error_code& error )
{
    std::lock_guard<std::mutex> lock( _impl->mutex() );
    connections_vector& connections = _impl->connections();
    auto iter = std::find( connections.begin(), connections.end(), connection );
    if( iter != connections.end() )
        connections.erase( iter );
}


void Server::connect_complete( Connection::ptr connection, const system::error_code& error )
{
    if( error )
    {
        std::cerr << "error: client connection failed" << std::endl;
        return;
    }
    
    std::unique_lock<std::mutex> lock( _impl->mutex() );
    _impl->connections().push_back( connection );
    lock.unlock();
    
    connection->work_complete.connect( boost::bind( &Server::handle_complete, this, _1, _2 ) );
    connection->handle();

    Connection::ptr new_connection = Connection::new_instance( _impl->handler() );
    _impl->acceptor()->async_accept( *new_connection->socket(), boost::bind( &Server::connect_complete, this, new_connection, _1 ) );
}


void Server::start( const std::string& addr_str, const std::string& port_str )
{
	ip::address addr;
	if( addr_str != "localhost" )
	{
		error_code err;
		addr = ip::address::from_string( addr_str );
		if( err ) 
		{
			std::cerr << "error: address is invalid" << std::endl;
			return;
		}
	}
	else
		addr = ip::address::from_string( "127.0.0.1" );
	
	std::stringstream ss( port_str );
    unsigned short port;
    ss >> port;
    if( ss.fail() )
	{
        std::cerr << "error: port is invalid" << std::endl;
		return;
    }
    
    ip::tcp::endpoint ep( addr, port );
	_impl->acceptor()->open( ep.protocol() );

	error_code err;    
    _impl->acceptor()->bind( ep, err );
    if( err )
    {
        std::cerr << "error: local address can not bind" << std::endl;
        return;
    }
    _impl->acceptor()->listen();
	
    Connection::ptr connection = Connection::new_instance( _impl->handler() );
    _impl->acceptor()->async_accept( *connection->socket(), boost::bind( &Server::connect_complete, this, connection, _1 ) );
	
    std::cout << "server started! ip: " << ep << std::endl;
    
    _impl->service()->run();
}

io_service_ptr Server::service() 
{ 
    return _impl->service();
}