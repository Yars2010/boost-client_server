

#ifndef SERVER_H
#define SERVER_H


#include <boost/asio/io_service.hpp>
#include <memory>

#include "connection.h"
#include "handler.h"


namespace Program
{
    
    using namespace boost;
    using namespace boost::asio;
    
    
    class ServerImpl;
    
    
    typedef std::shared_ptr<ServerImpl> ServerImplPtr;
    typedef std::shared_ptr<io_service> io_service_ptr;
    
    
    class Server: std::enable_shared_from_this<Server>
    {
    public:
        typedef std::shared_ptr<Server> ptr;
        typedef std::weak_ptr<Server> weak_ptr;
    
        Server();
        Server( const Server& server ) = delete;
        
        void start( const std::string& addr_str, const std::string& port_str );
        io_service_ptr service();
        
    private:
        void handle_complete( Connection::ptr connection, const system::error_code& error );
        void connect_complete( Connection::ptr connection, const system::error_code& error );
    
        ServerImplPtr _impl;
    };
    
    
} // namespace Program


#endif // SERVER_H
