##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release
ProjectName            :=server
ConfigurationName      :=Release
WorkspacePath          :=/home/silver-boy/Source/cpp/boost-client_server
ProjectPath            :=/home/silver-boy/Source/cpp/boost-client_server/server
IntermediateDirectory  :=./release
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=silverboy
Date                   :=12/09/19
CodeLitePath           :=/home/silver-boy/.codelite
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)NDEBUG 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="server.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch).. 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)pthread 
ArLibs                 :=  "pthread" 
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -O2 -Wall $(Preprocessors)
CFLAGS   :=  -O2 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/server.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_read_records.cpp$(ObjectSuffix) $(IntermediateDirectory)/connection.cpp$(ObjectSuffix) $(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IntermediateDirectory)/handler.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./release || $(MakeDirCommand) ./release


$(IntermediateDirectory)/.d:
	@test -d ./release || $(MakeDirCommand) ./release

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/server.cpp$(ObjectSuffix): server.cpp $(IntermediateDirectory)/server.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/boost-client_server/server/server.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/server.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/server.cpp$(DependSuffix): server.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/server.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/server.cpp$(DependSuffix) -MM server.cpp

$(IntermediateDirectory)/server.cpp$(PreprocessSuffix): server.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/server.cpp$(PreprocessSuffix) server.cpp

$(IntermediateDirectory)/up_read_records.cpp$(ObjectSuffix): ../read_records.cpp $(IntermediateDirectory)/up_read_records.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/boost-client_server/read_records.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_read_records.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_read_records.cpp$(DependSuffix): ../read_records.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_read_records.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_read_records.cpp$(DependSuffix) -MM ../read_records.cpp

$(IntermediateDirectory)/up_read_records.cpp$(PreprocessSuffix): ../read_records.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_read_records.cpp$(PreprocessSuffix) ../read_records.cpp

$(IntermediateDirectory)/connection.cpp$(ObjectSuffix): connection.cpp $(IntermediateDirectory)/connection.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/boost-client_server/server/connection.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/connection.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/connection.cpp$(DependSuffix): connection.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/connection.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/connection.cpp$(DependSuffix) -MM connection.cpp

$(IntermediateDirectory)/connection.cpp$(PreprocessSuffix): connection.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/connection.cpp$(PreprocessSuffix) connection.cpp

$(IntermediateDirectory)/main.cpp$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/boost-client_server/server/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.cpp$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/main.cpp$(DependSuffix) -MM main.cpp

$(IntermediateDirectory)/main.cpp$(PreprocessSuffix): main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.cpp$(PreprocessSuffix) main.cpp

$(IntermediateDirectory)/handler.cpp$(ObjectSuffix): handler.cpp $(IntermediateDirectory)/handler.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/silver-boy/Source/cpp/boost-client_server/server/handler.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/handler.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/handler.cpp$(DependSuffix): handler.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/handler.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/handler.cpp$(DependSuffix) -MM handler.cpp

$(IntermediateDirectory)/handler.cpp$(PreprocessSuffix): handler.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/handler.cpp$(PreprocessSuffix) handler.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./release/


